import React, { Component } from 'react';

import './App.css';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import Components from './components/Contents';
import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Header/>
          <Sidebar/>
          <Components/>
          <Footer/>
      </div>
    );
  }
}

export default App;
