import React from 'react';

const Sidebar = () => {
    return(
        <div className="colRight">
            <div className="text">
                <h1>Текст сайтбара</h1>
                <p>Содержимое сайтбара</p>
            </div>
        </div>
    )
};

export default Sidebar;