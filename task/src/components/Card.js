import React from 'react';

const Card = (props) => {
    return(
            <div className="card container">
                <h2 className="title">{props.title}</h2>
                <p className="year">{props.year}</p>
                <img className='img' src={props.img} alt=""/>
                <a className='link' href={props.link}>ссылка на фильм</a>
            </div>

    )
};
export default Card;