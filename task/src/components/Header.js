import React from 'react';

const Header = () => {
    return(
    <header className="container clearfix">
        <a className="name" href="#">Header...</a>
        <nav className="main-nav">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Prices</a></li>
                <li><a href="#">Contacts</a></li>
            </ul>
        </nav>
    </header>
    )
};

export default Header;